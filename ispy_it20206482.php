<?php


class Ispy {
	var $log_file = "logs";
	var $telegram_notice = true;
	var $telegram_api = "5929318893:AAFtMdGS6QQ_MgFlCltorumejpYel6Fji54";
	var $telegram_chatid = "849305317";
	var $protect_unset_global = true;
	var $protect_santy = true;
	var $protect_request_method = true;
	var $protect_dos = true;
	var $protect_union_sql = true;
	var $protect_xss = true;
	var $protect_cookies = true;
	var $protect_post = true;
	var $protect_get = true;
	var $protect_upload = true;
	var $protect_upload_maxsize = 25; // Max file size in mb

	private function msgWarning($msg) {
		$show = "<title>Blocked</title>
		<center>
			 ".$msg." ]</b></i>
		</center>";
		return $show;
	}

	function sendNotification($chatid, $text) {
		return file_get_contents("https://api.telegram.org/bot".$this->telegram_api."/sendMessage?chat_id=".$chatid."&text=".$text);
	}

	private function unset_globals() {
		if ( ini_get('register_globals') ) {
			$allow = array('_ENV' => 1, '_GET' => 1, '_POST' => 1, '_COOKIE' => 1, '_FILES' => 1, '_SERVER' => 1, '_REQUEST' => 1, 'GLOBALS' => 1);
			foreach ($GLOBALS as $key => $value) {
				if ( ! isset( $allow[$key] ) ) unset( $GLOBALS[$key] );
			}
		}
	}

	private function get_env($st_var) {
		global $HTTP_SERVER_VARS;
		if(isset($_SERVER[$st_var])) {
			return strip_tags( $_SERVER[$st_var] );
		} elseif(isset($_ENV[$st_var])) {
			return strip_tags( $_ENV[$st_var] );
		} elseif(isset($HTTP_SERVER_VARS[$st_var])) {
			return strip_tags( $HTTP_SERVER_VARS[$st_var] );
		} elseif(getenv($st_var)) {
			return strip_tags(getenv($st_var));
		} elseif(function_exists('apache_getenv') && apache_getenv($st_var, true)) {
			return strip_tags(apache_getenv($st_var, true));
		}
		return '';
	}

	private function get_referer() {
		if( $this->get_env('HTTP_REFERER') )
			return $this->get_env('HTTP_REFERER');
		return 'no referer';
	}

	private function get_ip() {
		if ($this->get_env('HTTP_X_FORWARDED_FOR')) {
			return $this->get_env('HTTP_X_FORWARDED_FOR');
		} elseif ($this->get_env('HTTP_CLIENT_IP')) {
			return $this->get_env('HTTP_CLIENT_IP');
		} else {
			return $this->get_env('REMOTE_ADDR');
		}
	}

	private function get_user_agent() {
		if($this->get_env('HTTP_USER_AGENT'))
			return $this->get_env('HTTP_USER_AGENT');
		return 'none';
	}

	private function get_query_string() {
		if($this->get_env('QUERY_STRING'))
			return str_replace('%09', '%20', $this->get_env('QUERY_STRING'));
		return '';
	}

	private function get_request_method() {
		if($this->get_env('REQUEST_METHOD'))
			return $this->get_env('REQUEST_METHOD');
		return 'none';
	}



	private function logs($type) {
		$f = fopen(dirname(__FILE__).'/'.$this->log_file.'.txt', 'a');
		$msg = date('j-m-Y H:i:s')." | $type | IP: ".$this->get_ip()." | DNS: ".gethostbyaddr($this->get_ip())." | Agent: ".$this->get_user_agent()." | URL: ".$_SERVER['REQUEST_URI']." | Referer: ".$this->get_referer();
		fputs($f, $msg."\n\n");
		fclose($f);
		if($this->telegram_notice === true) {
			$this->sendNotification($this->telegram_chatid, $msg);
			$this->sendSMS($this->$msg);
		}
	}

	private function check_upload() {
		$f_uploaded = array();
		$f_uploaded = $this->fetch_uploads();
		$tmp = '';
		if ( $this->protect_upload == false ) {
			$tmp = '';
			foreach ($f_uploaded as $key => $value) {
				if (! $f_uploaded[$key]['name']) { continue; }
				$tmp .= $f_uploaded[$key]['name'] . ' (' . number_format($f_uploaded[$key]['size']) . ' bytes) ';
	      }
	      if ( $tmp ) {
				$this->logs('Blocked file upload attempt', rtrim($tmp, ' '), 3, 0);
				die($this->msgWarning('Unauthorized file upload detected ! stop it ...'));
			}
		} else {
			foreach ($f_uploaded as $key => $value) {
				if (! $f_uploaded[$key]['name']) { continue; }
				if ( $f_uploaded[$key]['size'] > $this->protect_upload_maxsize ) {
					$this->logs('Attempt to upload a file > ' . ($this->protect_upload_maxsize / 1024) .
						' KB' , $f_uploaded[$key]['name'] . ' (' . number_format($f_uploaded[$key]['size']) . ' bytes)', 1, 0);
					die($this->msgWarning('Unauthorized file upload detected ! stop it ...'));
				}
				$data = '';

				if (preg_match('/\.ht(?:access|passwd)|(?:php\d?|\.user)\.ini|\.ph(?:p([34x7]|5\d?)?|t(ml)?)(?:\.|$)/', $f_uploaded[$key]['name']) ) {
					$this->logs('Attempt to upload a script or system file', $f_uploaded[$key]['name'] . ' (' . number_format($f_uploaded[$key]['size']) . ' bytes)', 3, 0);
					die($this->msgWarning('Unauthorized file upload detected ! stop it ...'));
				}
				$data = file_get_contents($f_uploaded[$key]['tmp_name']);

				if (preg_match('`^\x7F\x45\x4C\x46`', $data) ) {
					$this->logs('Attempt to upload an executable file (ELF)', $f_uploaded[$key]['name'] . ' (' . number_format($f_uploaded[$key]['size']) . ' bytes)', 3, 0);
					die($this->msgWarning('Unauthorized file upload detected ! stop it ...'));
				}
					// MZ header :
				if (preg_match('`^\x4D\x5A`', $data) ) {
					$this->logs('Attempt to upload an executable file (Microsoft MZ header)', $f_uploaded[$key]['name'] . ' (' . number_format($f_uploaded[$key]['size']) . ' bytes)', 3, 0);
					die($this->msgWarning('Unauthorized file upload detected ! stop it ...'));
				}


				if (preg_match('`(<\?(?i:php\s|=[\s\x21-\x7e]{10})|#!/(?:usr|bin)/.+?\s|\s#include\s+<[\w/.]+?>|\W\$\{\s*([\'"])\w+\2)`', $data, $match) ) {
					$this->logs('Attempt to upload a script', $f_uploaded[$key]['name'] . ' (' . number_format($f_uploaded[$key]['size']) . ' bytes), pattern: '. $match[1], 3, 0);
					die($this->msgWarning('Unauthorized file upload detected ! stop it ...'));
				}

				if ( preg_match( '`<svg.*>.*?(<[a-z].+?\bon[a-z]{3,29}\b\s*=.{5}|<script.*?>.+?</script\s*>|data:image/svg\+xml;base64|javascript:|ev:event=).*?</svg\s*>`s', $data, $match ) ) {
					$this->logs('Attempt to upload an SVG file containing Javascript/XML events', $f_uploaded[$key]['name'] . ' (' . number_format($f_uploaded[$key]['size']) . ' bytes), pattern: '. $match[1], 3, 0);
					die($this->msgWarning('Unauthorized file upload detected ! stop it ...'));
				}

				if ( $f_uploaded[$key]['size'] > 67 && $f_uploaded[$key]['size'] < 129 ) {
					if ( empty($data) ) {
						$data = file_get_contents( $f_uploaded[$key]['tmp_name'] );
					}

				}


				$this->logs('File upload detected, no action taken' . $tmp , $f_uploaded[$key]['name'] . ' (' . number_format($f_uploaded[$key]['size']) . ' bytes)', 5, 0);
			}
		}
	}

	private function fetch_uploads() {
		global $file_buffer, $upload_array, $prop_key;
		$upload_array = array();
		foreach( $_FILES as $f_key => $f_value ) {
			foreach( $f_value as $prop_key => $prop_value ) {
				// Fetch all but 'error':
				if (! in_array( $prop_key, array( 'name', 'type', 'tmp_name', 'size' ) ) ) { continue; }
				$file_buffer = $f_key;
				if ( is_array( $_FILES[$f_key][$prop_key] ) ) {
					$this->recursive_upload( $_FILES[$f_key][$prop_key] );
				} else {
					if (! empty( $_FILES[$f_key][$prop_key] ) ) {
						$upload_array[$f_key][$prop_key] = $_FILES[$f_key][$prop_key];
					}
				}
			}
		}
		return $upload_array;
	}

	private function recursive_upload( $data ) {
		global $file_buffer, $upload_array, $prop_key;
		foreach( $data as $data_key => $data_value ) {
			if ( is_array( $data_value ) ) {
				$file_buffer .= "_{$data_key}";
				$this->recursive_upload( $data_value );
			} else {
				if ( empty( $data_value ) ) { continue; }
				$upload_array["{$file_buffer}_{$data_key}"][$prop_key] = $data_value;
			}
		}
	}

	private function sanitize_filename( $array, $key, $value ) {
		array_walk_recursive(
			$array, function( &$v, $k ) use ( $key, $value ) {
				if (! empty( $v ) && $v == $key ) { $v = $value; }
			}
		);
		return $array;
	}

	public function secureMe($activate) {
		$regex_union = "'#\w?\s?union\s\w*?\s?(select|all|distinct|insert|update|drop|delete)#is'";
		if($activate == true) {
			$this->check_upload();
			if($this->protect_unset_global == true) {
				$this->unset_globals();
			}
			

			

			if($this->protect_cookies === true) {
				$ct_rules = Array('applet', 'base', 'bgsound', 'blink', 'embed', 'expression', 'frame', 'javascript', 'layer', 'link', 'meta', 'object', 'onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload', 'script', 'style', 'title', 'vbscript', 'xml');
				if($this->protect_cookies === true) {
					foreach($_COOKIE as $value) {
						$check = str_replace($ct_rules, '*', $value);
						if($value != $check) {
							$this->logs('Cookie protect');
							unset($value);
							die($this->msgWarning('Cookie attack detected ! stop it ...'));
						}
					}
				}
				if($this->protect_post === true) {
					foreach($_POST as $value) {
						$check = str_replace($ct_rules, '*', $value);
						if($value != $check) {
							$this->logs('POST protect');
							unset($value);
							die($this->msgWarning('POST attack detected ! stop it ...'));
						}
					}
				}
				if($this->protect_get === true) {
					foreach($_GET as $value) {
						$check = str_replace($ct_rules, '*', $value);
						if($value != $check) {
							$this->logs('GET protect');
							unset($value);
							die($this->msgWarning('GET attack detected ! stop it ...'));
						}
					}
				}
			}


		
			if ( $this->protect_dos === true ) {

				if ( $this->get_user_agent() == '-' ) {
					$this->logs( 'Dos attack' );
					die($this->msgWarning('Invalid user agent ! Stop it ...'));
				}
			}

			if ( $this->protect_union_sql === true ) {
				$stop = 0;
				$ct_rules = array( '*/from/*', '*/insert/*', 's', '%20into%20', '*/into/*', ' into ', 'into', '*/limit/*', 'not123exists*', '*/radminsuper/*', '*/select/*', '+select+', '%20select%20', ' select ',  '+union+', '%20union%20', '*/union/*', ' union ', '*/update/*', '*/where/*' );
				$check    = str_replace($ct_rules, '*', $this->get_query_string() );
				if( $this->get_query_string() != $check ) $stop++;
				if (preg_match($regex_union, $this->get_query_string())) $stop++;
				if (preg_match('/([OdWo5NIbpuU4V2iJT0n]{5}) /', rawurldecode( $this->get_query_string() ))) $stop++;
				if (strstr(rawurldecode( $this->get_query_string() ) ,'*')) $stop++;
				if ( !empty( $stop ) ) {
					$this->logs( 'Union attack' );
					die($this->msgWarning('SQL attack detected ! stop it ......'));
				}
			}



			if ( $this->protect_xss === true ) {
				$ct_rules = array( 'http\:\/\/', 'https\:\/\/', 'cmd=', '&cmd', 'exec', 'concat', './', '../',  'http:', 'h%20ttp:', 'ht%20tp:', 'htt%20p:', 'http%20:', 'https:', 'h%20ttps:', 'ht%20tps:', 'htt%20ps:', 'http%20s:', 'https%20:', 'ftp:', 'f%20tp:', 'ft%20p:', 'ftp%20:', 'ftps:', 'f%20tps:', 'ft%20ps:', 'ftp%20s:', 'ftps%20:', '.php?url=' );
				$check    = str_replace($ct_rules, '*', $this->get_query_string() );
				if( $this->get_query_string() != $check ) {
					$this->logs( 'XSS attack' );
					die($this->msgWarning('XSS attack detected ! stop it ...'));
				}
			}

		}
	}
}
